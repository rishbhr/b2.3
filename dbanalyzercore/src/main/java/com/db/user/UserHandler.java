package com.db.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;

public class UserHandler {
    private static final Logger LOGGER = Logger.getLogger("DBLogger");
    static  private UserHandler self = null;
    
    private UserHandler(){}
    
    public static UserHandler getUserHandler() {
        if( self == null )
            self = new UserHandler();
        return self;
    }
    
    public static String hashPwd(String pwd) {
        return Hashing.sha256()
                    .hashString("D68F3CDD84BA2DED9DC2E2C774383415" + pwd, StandardCharsets.UTF_8)
                    .toString();
    }
    
    public User loadFromDB( Connection conn, String userid, String pwd ) {
        User result = null;
        try {
            LOGGER.log(Level.INFO, "Starting to load user from DB");
            String sbQuery = "select * from users where user_id=?";
            PreparedStatement stmt = conn.prepareStatement(sbQuery);            
            stmt.setString(1, userid);
            ResultSet rs = stmt.executeQuery();
            
            if( rs.next() ) {
                String next_id = rs.getString("user_id");
                String next_pwd = rs.getString("user_pwd");
                String hashed_pwd = hashPwd(next_pwd);
                if( pwd.equals(hashed_pwd) ) {
                    result = new User(next_id, hashed_pwd);
                }
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.WARNING, "An exception occured during user search", ex);
        }
        LOGGER.log(Level.INFO, "Finished loading user from DB");
        return result;
    }
}