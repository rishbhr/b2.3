/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.dbanalyzercore;

import com.db.user.User;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Graduate
 */
public interface DBLoader {
    public List<String> getTables();
    public ResultSet execQuery(String query);
    public User loadFromDB(String userid, String pwd);
}
