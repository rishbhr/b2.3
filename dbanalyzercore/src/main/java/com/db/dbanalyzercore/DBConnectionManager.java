package com.db.dbanalyzercore;

import com.db.user.User;
import com.db.user.UserHandler;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public class DBConnectionManager implements DBLoader {
    private static DBConnectionManager self = null;
    private DBAnalyzerCore core = null;
    
    private DBConnectionManager() {
        core = new DBAnalyzerCore();
    }
    
    public static DBConnectionManager getConnectionManager() {
        if(self == null)
            self = new DBConnectionManager();
        return self;
    }
    
    public void establishConnection() {
        core.openConnection();
    }
    
    public DBAnalyzerCore getCore() {
        return core;
    }

    public Connection getConnection() {
        return core.conn;
    }
    
     public List<String> getTables() {
         return core.getTables();
    }
     
    public ResultSet execQuery(String query) {
        return core.execQuery(query);
    }

    @Override
    public User loadFromDB(String userid, String pwd) {
        User result;
        UserHandler handler = UserHandler.getUserHandler();
        result = handler.loadFromDB(core.getConnection(), userid, pwd);
        return result;
    }
}
