package com.db.dbanalyzercore;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBAnalyzerCore {
    private static final Logger LOGGER = Logger.getLogger("DBLogger");
    // JDBC driver name and database URL
    String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    //String DB_URL = "jdbc:mysql://127.0.0.1:3307/";  // to use locally
    //String DB_URL = "jdbc:mysql://10.0.75.1:3307/";    // to use inside docker
    String DB_URL = "jdbc:mysql://10.11.32.21:3306/"; // to use on deployment server

    //  Database credentials
    String DBNAME = "db_grad_cs_1917"; //db_grad";
    String DBUSER = "dbgrad"; //root";
    String DBPASS = "dbgrad";// ppp";

    Connection conn;
    public Connection getConnection() {
        return conn;
    }

    public DBAnalyzerCore() {
        try {
            // Register JDBC driver
            Class.forName(JDBC_DRIVER);
        } catch( ClassNotFoundException e ) {
            LOGGER.log(Level.SEVERE, "Cannot register JDBC driver", e);
        }
    }

    public void openConnection() {
        try {
            conn = DriverManager.getConnection(DB_URL + DBNAME, DBUSER, DBPASS);
        } catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "Cannot establish connection with database", e);
        }
    }
    
    public boolean isSuccessfullyConnected() {
        return conn != null;
    }

    public ResultSet execQuery(String query) {
        ResultSet rs = null;
        if( !isSuccessfullyConnected() )
            return rs;
        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException e ) {
            LOGGER.log(Level.WARNING, "An error occured while executing database query", e);
        }
        return rs;
    }

    public List<String> getTables() {
        List<String> result = new ArrayList<>();
        if( !isSuccessfullyConnected() )
            return result;
        try {
            DatabaseMetaData md = conn.getMetaData();
            ResultSet rs = md.getTables(DBNAME, DBNAME, "%", new String[] {"TABLE"});
            while( rs.next() ) {
                result.add(rs.getString(3));
            }
        } catch( SQLException e ) {
            LOGGER.log(Level.WARNING, "An error occured while retrieving list of tables", e);
        }
        return result;
    }

    public void closeConnection() {
        if( !isSuccessfullyConnected() )
            return;
        try {
            conn.close();
        } catch(SQLException e) {
            LOGGER.log(Level.WARNING, "An error occured while closing connection", e);
        }
    }  
}