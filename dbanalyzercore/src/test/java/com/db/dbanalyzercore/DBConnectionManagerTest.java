/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.dbanalyzercore;

import com.db.user.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.jmock.Mockery;
import org.jmock.Expectations;

/**
 *
 * @author Graduate
 */
public class DBConnectionManagerTest {
    static Mockery context;
    
    public DBConnectionManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        context = new Mockery();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadFromDB method, of class DBConnectionManager.
     */
    @Test
    public void testLoadFromDB() {
        System.out.println("loadFromDB");
        String userid = "user";
        String pwd = "password";
        DBLoader instance = context.mock(DBLoader.class);
        User expResult = new User(userid, pwd);
        
        context.checking(new Expectations() {{
            oneOf(instance).loadFromDB(userid, pwd);
                will(returnValue(new User(userid, pwd)));
        }});
        instance.loadFromDB(userid, pwd);
        context.assertIsSatisfied();
    }
    
}
