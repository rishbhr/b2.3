package com.db.dbanalyzerwebfront.web;

import com.db.dbanalyzercore.DBConnectionManager;
import com.db.dbanalyzercore.DBLoader;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import com.db.user.User;
import com.db.user.UserHandler;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/services")
public class JerseyBackend  {
    private DBLoader dbManager = null;
    
    @Context
    HttpServletRequest req;
    
    public void setDBManager(DBLoader dbManager) {
        this.dbManager = dbManager;
    }
    
    @POST
    @Path("/login")
    public Response login(@FormParam("usr")String userid, @FormParam("pwd")String pwd) {
        if(dbManager == null) {
            dbManager = DBConnectionManager.getConnectionManager();
        }
        String hashed_pwd = UserHandler.hashPwd(pwd);
        User user = dbManager.loadFromDB(userid, hashed_pwd);
        
        if( user != null) {
            HttpSession session = req.getSession(true);
            session.setAttribute("user", user.getName());
            return Response.ok(user, MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            return Response.status(400).entity("User could not be found").build();
        }
    }
    
    @POST
    @Path("/logout")
    public Response logout() {
        HttpSession session = req.getSession(true);
        session.setAttribute("user", null);
        return Response.status(200).entity("User successfully logged out").build();
    }
    
    @GET
    @Path("/dbactions/list_tables")
    public Response listTables() {
        HttpSession session = req.getSession(true);
        if(session.getAttribute("user") == null) {
            return Response.status(400).entity("You are not logged in").build();
        }
        if(dbManager == null) {
            dbManager = DBConnectionManager.getConnectionManager();
        }
        List<String> tables = dbManager.getTables();
            if( tables != null) {
                return Response.ok(tables, MediaType.APPLICATION_JSON_TYPE).build();
            } else {
                return Response.status(400).entity("Could not load table list").build();
            }
    }
    
    @GET
    @Path("/dbactions/fetch_data")
    public Response fetchData(@QueryParam("table") String table, @QueryParam("rowFfrom") int rowFrom, @QueryParam("rowTo") int rowTo) {
        HttpSession session = req.getSession(true);
        if(session.getAttribute("user") == null) {
            return Response.status(400).entity("You are not logged in").build();
        }
        if(dbManager == null) {
            dbManager = DBConnectionManager.getConnectionManager();
        }
        ResultSet data = dbManager.execQuery("SELECT * FROM " + table + " LIMIT " + rowFrom + ", " + rowTo + ";");
        if( data != null) {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory f = mapper.getFactory();
            ByteArrayOutputStream jsonStream = new ByteArrayOutputStream();
            try {
                JsonGenerator g = f.createGenerator(jsonStream);
                int nCols = data.getMetaData().getColumnCount();
                g.writeStartArray();
                while(data.next()) {
                    g.writeStartObject();
                    for(int i = 0; i < nCols; ++i) {
                        String columnName = data.getMetaData().getColumnLabel(i + 1);
                        String columnType = data.getMetaData().getColumnClassName(i + 1);
                        switch(columnType) {
                            case "java.lang.Integer":
                                g.writeNumberField(columnName, data.getInt(i + 1));
                                break;
                            case "java.lang.Double":
                                g.writeNumberField(columnName, data.getFloat(i + 1));
                                break;
                            default:
                                g.writeStringField(columnName, data.getString(i + 1));
                                break;
                        }
                    }
                    g.writeEndObject();
                }
                g.writeEndArray();
                g.close();
                return Response.ok(new String(jsonStream.toByteArray(), "UTF-8")).build();
            } catch (IOException | SQLException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                return Response.status(400).entity("Could not convert data to JSON, the following exception occured: " + sw.toString()).build();
            }
        } else {
            return Response.status(400).entity("Could not fetch data").build();
        }
    }
    
}