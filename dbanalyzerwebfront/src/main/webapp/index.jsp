<!DOCTYPE html>
<html>
    <%@ page import="java.sql.*,java.util.List,com.db.dbanalyzercore.DBConnectionManager" %>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/main.js"></script> 
    </head>
    <body>
        <%! DBConnectionManager db = DBConnectionManager.getConnectionManager(); %>
        <%
            db.establishConnection();
            if( !db.getCore().isSuccessfullyConnected() ) {
        %>
        <h2>DB not connected</h2>
        <%
            } else {
        %>
        <div class="vertical-center" id="loginPageDiv" <%=session.getAttribute("user") != null ?  "style=\"display:none;\"" : ""%>>
            <div class="container">
                <h3 align="center">Database successfully connected</h3>
                <h3 class="loginPage" align="center">Please log into the system</h3>
                <form class="form-horizontal" id="loginForm">
                    <div class="form-group row">
                        <label class="col-form-row control-label col-sm-2" for="usr">Username:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="f_userid" name="usr" placeholder="User login">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-row control-label col-sm-2" for="pwd">Password:</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="f_pwd" name="pwd" placeholder="Password">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>            
            </div>
        </div>
        
        <%
            }
        //    db.getCore().closeConnection();
        %>
            
        <%
            if(session.getAttribute("user") != null) {
        %>
            <div id="userIdMessage">
                <%= "You loggen in as " + session.getAttribute("user")%>
            </div>
        <%
            }
        %>
        
        <select name="Table: " id="tables_list" style='<%=session.getAttribute("user") == null ? "display:none;" : "display:block;"%>'>
            <option>Select table name</option>
        </select>
            
        <!-- HTML -->
        <div class="table" id="tableGoesHere">
        </div>

    </body>
</html>