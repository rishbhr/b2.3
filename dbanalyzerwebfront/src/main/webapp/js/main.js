var rootURL = "dba/services";

$(document).ready(function() {
	$( "#loginForm" ).submit(function( event ) {
		loginFromForm();
		event.preventDefault();
	});
        $("#tables_list").change(displayValue);
        if(sessionStorage.getItem("login")) {
            listTables();
        }
});

function loginFromForm() {
    formdata = $('form');
    fds = formdata.serialize();
    var request = $.ajax({
		method: 'POST',
		url: rootURL + '/login',
		dataType: "json", // data type of response
		data: fds,
                
		success: function (result) {
                    $( 'form' ).trigger("reset");  // reset the form
                    $( ".loginPage" ).css({"display" : "none"});
                    $("#userIdMessage").html("<p>Login successfull, result from the server: " + JSON.stringify(result) + "</p>");
                    sessionStorage.setItem("login", true);
                    handleSuccessfulLogin();
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown ) {
        alert( "Request " + textStatus + ", invalid login details: " + fds );
        $("#userIdMessage").html("");
    });
}

function handleSuccessfulLogin() {
    $("#loginPageDiv").remove();
    $("#tables_list").css({"display" : "block"});
    listTables();
    
}

function listTables() {
    var request = $.ajax({
		method: 'GET',
		url: rootURL + '/dbactions/list_tables',
		dataType: "json", // data type of response
                
		success: function (result) {
                    $.each(result, function(t) {
                        $("#tables_list").append("<option>" + this + "</option>");
                    });
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        console.log( "Request " + textStatus + ", response: " + jqXHR.responseText);
    });
}

function fetchTable(table_name) {
    if(table_name === "Select table name"){
        
    } else {
        var request = $.ajax({
            method: 'GET',
            url: rootURL + '/dbactions/fetch_data',
                    data : {
                        "table" : table_name,
                        "rowFrom" : 0,
                        "rowTo" : 10
                    },
            dataType: "json", // data type of response
                    
            success: function (result) {
                        //$("#raw_data").html("<p>" + JSON.stringify(result) + "</p>");
                        //var result_string = JSON.stringify(result);
                        //var result_object = JSON.parse(result_string);
                        outreach_table(result);
            }
        });
        request.fail(function( jqXHR, textStatus, errorThrown )
        {
            alert( "Request " + textStatus + ", error message: " + errorThrown + ", jqXHR.responseText: " + jqXHR.responseText);
        });
    }
}

function logout_handler(){
    function logout_handler(){
        // send post request to logout jersey backend
        var request = $.ajax({
            method: 'POST',
            url:rootURL + '/dbactions/logout',
                success: function() {
                    $( 'lg_out' ).trigger("reset");  // reset the form
                        $( ".logoutPage" ).css({"display" : "none"});
                        document.writeln("<p>Logout successfull, result from the server</p>");
                }
        });
        request.fail(function( jqXHR, textStatus, fds){
            document.writeln(jqXHR, textStatus, fds)
        });
    }
}

function outreach_table(data) {
    build_table(data);
}

// build table
function build_table(myJson, classes) {
    var cols = Object.keys(myJson[0]);
    var headerRow = '';
    var bodyRows = '';

    classes = classes || '';

    cols.map(function(col) {
        headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
    });

    myJson.map(function(row) {
        bodyRows += '<tr>';

        cols.map(function(colName) {
            bodyRows += '<td>' + row[colName] + '</td>';
            console.log(bodyRows);
        });

        bodyRows += '</tr>';
    });

    var my_table =  '<table class="myTable" " + classes + "><thead><tr>' + headerRow + '</tr></thead><tbody>' + bodyRows + '</tbody></table>';    
    $("#tableGoesHere").html(my_table);

}

// addition to the table function
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function displayValue() {
    singleValue = $("#tables_list").val();
    fetchTable(singleValue);
    $('.myTable').find('td').first().remove();
}

